const express = require('express');
const userController = require('../controllers/userController');
const router = express.Router();

const verifyToken = require('../middlewares/auth.middleware');

router.post('/register', userController.createUser);
router.post('/signin', userController.signinUser);

module.exports = router;
