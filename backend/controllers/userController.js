const Users = require('../models/user.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// Creating/Registering the user
// email -> ross@taylor.com, password -> qwerty11
exports.createUser = (req, res, next) => {
  const saltRounds = 10;
  bcrypt.hash(req.body.password, saltRounds, function(error, hashPassword) {
    if(hashPassword) {
      const userData = new Users({
        firstname: req.body.firstname.toLowerCase(),
        lastname: req.body.lastname.toLowerCase(),
        email: req.body.email.toLowerCase(),
        phone: req.body.phone,
        password: hashPassword
      });
      console.log(userData);
      Users.findOne({ email: req.body.email }).then(user => {
        if(user) {
          res.status(200).json({
            message: "User already exists!!"
          })
          console.log("User already exists!!")
        }
        else {
          userData.save();
          res.status(200).json({
            message: "User register successfully!!"
          });
          console.log("User register successfully!!")
        }
      })
    } else {
      res.status(500).json({
        message: "Inrternal error."
      })
    }
  });
};

exports.signinUser = (req, res, next) => {
  Users.findOne({ email: req.body.email}).then(user => {
    if(!user) {
      console.log("Not found!!", user);
      res.status(200).json({
        message: "No user found!!"
      })
    } else {
      console.log("Found", user);
      bcrypt.compare(req.body.password, user.password, function(error, response) {
        if(!response) {
          console.log("Incorrect password");
          res.status(200).json({
            message: "Incorrect password"
          })
        } else {
          console.log("Correct password");
          const userData = {
            id: user._id,
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
          }
          jwt.sign({ user: userData }, 'this_is_my_secret_authenticating_key', { expiresIn: '1h' }, (err, token) => {
            console.log('token', token);
            if(err) {
              res.status(403).json({
                message: "Internal error."
              })
            } else {
              res.status(200).json({
                token: token,
                expiresIn: 3600,
                message: "User signin successfully!!"
              })
            }
          })
        }
      })
    }
  })
}
