import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root'})

export class CommonService {
  private appStore: Map<string, any> = new Map<string, any>();
  constructor() {}

  public setValue(key: string, value: any): void {
    this.appStore.set(key, value);
  }

  public getValue(key: string): any {
    return this.appStore.get(key);
  }

  public removeItem(key: string) {
    return this.appStore.delete(key);
  }

  public clearStore() {
    this.appStore.clear();
  }

}
