import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  hide = true;
  registerForm: FormGroup;
  private subscription: Subscription = new Subscription();

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.minLength(8)])
    });
  }

  onRegister() {
    const uForm: any = {
      firstname: this.registerForm.value.firstname,
      lastname: this.registerForm.value.lastname,
      email: this.registerForm.value.email,
      phone: this.registerForm.value.phone,
      password: this.registerForm.value.password
    };
    console.log('uform', uForm);
    this.subscription.add(
      this.authService.saveUser(uForm).subscribe(resMessage => {
        alert(resMessage.message + ' Please signin to continue.');
        this.router.navigate(['/investor/signin']);
        // console.log(resMessage.message, 'happy res');
      })
    );
  }

  // Unsubscribing all the subscription
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
