import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root'})

export class AuthService {
  private tokenTimer: any;
  constructor( private http: HttpClient, private router: Router ) {}

  isLoggedin(): boolean {
    const token = localStorage.getItem('currentUserToken');
    if (token) {
      return true;
    } else {
      return false;
    }
  }

  public saveUser(userData: any): Observable<any> {
    return this.http.post('http://localhost:3000/api/user/register', userData).pipe(map(data => {
        return data || {};
      })
    );
  }

  public signinUser(signinData: any) {
    return this.http.post<{ token: string, expiresIn: number , message: string}>('http://localhost:3000/api/user/signin', signinData)
    .pipe(map(data => {
      const token = data.token;
      if (token) {
        localStorage.setItem('currentUserToken', data.token);
        const expiresInDuration = data.expiresIn;
        this.tokenTimer = setTimeout(() => {
          this.logout();
        }, expiresInDuration * 1000);
        return data || {};
      }
    }));
  }

  public logout() {
    localStorage.clear();
    this.router.navigate(['/']);
    clearTimeout(this.tokenTimer);
  }
}
