import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const userToken = localStorage.getItem('currentUserToken');
    const authRequest = req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + userToken)
    });
    return next.handle(authRequest);
  }
}
