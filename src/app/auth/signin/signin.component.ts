import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['../register/register.component.css']
})
export class SigninComponent implements OnInit, OnDestroy {
  hide = true;
  signinForm: FormGroup;
  private subscription: Subscription = new Subscription();

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.signinForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)])
    });
  }

  onSignin() {
    const signinForm = {
      email: this.signinForm.value.email,
      password: this.signinForm.value.password
    };
    console.log('signinForm', signinForm);
    this.subscription.add(
      this.authService.signinUser(signinForm).subscribe(response => {
        // alert(response.message);
        this.router.navigate(['/']);
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
